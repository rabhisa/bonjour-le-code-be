FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD /target/*.jar app.jar
# add my . env to container base directory
COPY .env .
EXPOSE 8080
CMD ["-jar","/app.jar"]
ENTRYPOINT [ "java" , "-jar", "app.jar"]
