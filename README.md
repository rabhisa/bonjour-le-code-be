#Bonjour le code BE:

Description: back-end API for bonjour le code's project

API doc: http://localhost:8080/swagger-ui/

Example: http://localhost:8080/swagger-ui/#/auth-controller


How to use:
1. Update .env file based on your preferences (also please use logical values, for example for max length)
    N.B: for list values they should be separated by a comma ","
   
2. From project directory run: docker-compose up

[comment]: <> (TODO: update me once this is changed)

3. Using your fav rest client (insomnia, postman ...) and being guided by the rest docs (swagger), you can fiddle with existing api and try different scenarios
   

Example of pageable URL :
{{base}}/api/v1/app/list-photos?page=0&size=3&sort=name