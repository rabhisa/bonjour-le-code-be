package com.sysdream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@RestController
@EnableSwagger2
@EnableScheduling
public class BonjourLeCodeBEApplication {
    // this endpoint will give me data about the current logged in user
    @GetMapping("/user")
    public UsernamePasswordAuthenticationToken userInfo(UsernamePasswordAuthenticationToken user) {
        return user;
    }

    public static void main(String[] args) {
        SpringApplication.run(BonjourLeCodeBEApplication.class, args);
    }
}