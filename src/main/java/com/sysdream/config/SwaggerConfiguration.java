package com.sysdream.config;

import com.fasterxml.classmate.TypeResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

@Configuration
public class SwaggerConfiguration {

    @Bean
    public Docket api(final TypeResolver typeResolver) {
        return new Docket(DocumentationType.SWAGGER_2)
                .host("localhost:8080/bonjour-le-code-be")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.sysdream.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "My REST API for 'bonjour le code' project",
                "Some description of API controllers and their models.",
                "API 0.1",
                "Terms of service",
                new Contact("Salim RABHI", "www.sysdream.com", "rabhisalim9@gmail.com"),
                "License of API",
                "API license URL",
                Collections.emptyList());
    }
}