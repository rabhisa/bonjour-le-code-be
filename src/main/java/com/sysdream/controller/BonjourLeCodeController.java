package com.sysdream.controller;

import com.github.javafaker.Faker;
import com.sysdream.dto.ResponseDto;
import com.sysdream.model.Image;
import com.sysdream.model.TodaysData;
import com.sysdream.repo.ImageRepository;
import com.sysdream.repo.TodaysDataRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/app")
public class BonjourLeCodeController {
    private final static String about = "This is a test for sys dream, it consists of managing photos and display new photo daily";
    private final ImageRepository imageRepository;
    private final TodaysDataRepository todaysDataRepository;

    public BonjourLeCodeController(ImageRepository imageRepository, TodaysDataRepository todaysDataRepository) {
        this.imageRepository = imageRepository;
        this.todaysDataRepository = todaysDataRepository;
    }

    @GetMapping("/about")
    public ResponseEntity<?> getAbout() {
        return new ResponseEntity(new ResponseDto(Boolean.TRUE, about, "getAbout", HttpStatus.ACCEPTED), HttpStatus.ACCEPTED);
    }

    @GetMapping("/list-photos")
    public ResponseEntity<?> listPhotos(@NotNull final Pageable pageable) {
        Page<Image> images = imageRepository.findAll(pageable);
        return new ResponseEntity(new ResponseDto(Boolean.TRUE, images, "listPhotos", HttpStatus.ACCEPTED), HttpStatus.ACCEPTED);
    }

    @GetMapping("/todays-data")
    public ResponseEntity getTodaysData() {
        TodaysData todaysData = null;
        Optional<TodaysData> todaysDataOptional = todaysDataRepository.findFirstByOrderByCreatedDateDesc();
        if (todaysDataOptional.isPresent())
            todaysData = todaysDataOptional.get();
        return new ResponseEntity(new ResponseDto(Boolean.TRUE, todaysData, "getTodaysData", HttpStatus.ACCEPTED), HttpStatus.ACCEPTED);
    }

    @PostMapping("/generate-todays-data")
    public ResponseEntity generateTodaysData() {
        Faker faker = new Faker();
        LocalDateTime localDateTime = LocalDateTime.now();
        LocalDate localDate = localDateTime.toLocalDate();
        String localDateAsString = localDate.toString();
        List<Image> images = imageRepository.findAll();
        TodaysData todaysData = TodaysData.builder()
                .dateAsString(localDateAsString)
                .todaysFact(faker.chuckNorris().fact())
                .image(images.size() > 0 ? images.get(faker.random().nextInt(images.size())) : null)
                .build();

        todaysDataRepository.save(todaysData);
        return new ResponseEntity(new ResponseDto(Boolean.TRUE, todaysData, "generateTodaysData", HttpStatus.ACCEPTED), HttpStatus.ACCEPTED);
    }
}
