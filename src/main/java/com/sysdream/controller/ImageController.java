package com.sysdream.controller;

import com.sysdream.dto.ReviewImageDto;
import com.sysdream.service.interfaces.ImageService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;

@RestController
@RequestMapping(path = "/api/v1/images")
public class ImageController {
    private final ImageService imageService;

    public ImageController(ImageService imageService) {
        this.imageService = imageService;
    }

    @PostMapping()
    public ResponseEntity uploadImage(Authentication authentication, @RequestParam("imageFile") @Valid MultipartFile file) throws IOException {
        return imageService.uploadImage(authentication, file);
    }

    @GetMapping("")
    public ResponseEntity listImagesByUser(Authentication authentication) throws IOException {
        return imageService.listImagesByUser(authentication);
    }

    @GetMapping("/all")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity listAllImages(Authentication authentication) throws IOException {
        return imageService.listAllImages();
    }

    @GetMapping("{imageId}")
    public ResponseEntity getImage(Authentication authentication, @PathVariable("imageId") Long imageId) throws IOException {
        return imageService.getImage(authentication, imageId);
    }

    @PutMapping("{imageId}")
    public ResponseEntity updateImage(Authentication authentication, @PathVariable("imageId") Long imageId, @RequestParam("imageFile") @Valid MultipartFile file) throws IOException {
        return imageService.updateImage(authentication, imageId, file);
    }

    @DeleteMapping("{imageId}")
    public ResponseEntity deleteImage(Authentication authentication, @PathVariable("imageId") Long imageId) throws IOException {
        return imageService.deleteImage(authentication, imageId);
    }


    @PutMapping("review/{imageId}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity reviewImage(@PathVariable("imageId") Long imageId, @RequestParam("imageFile") @Valid ReviewImageDto dto) throws IOException {
        return imageService.reviewImage(imageId, dto);
    }

}