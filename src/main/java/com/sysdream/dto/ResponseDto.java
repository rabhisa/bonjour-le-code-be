package com.sysdream.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@NoArgsConstructor
@Builder
public class ResponseDto {
    private Boolean success;
    private Object data;
    private String error;
    private String message;
    private HttpStatus status;

    public ResponseDto(Boolean success) {
        this.success = success;
    }

    public ResponseDto(String message, Boolean success) {
        this.message = message;
        this.success = success;
    }
    public ResponseDto( Boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public ResponseDto(Boolean success, Object data, String error, String message, HttpStatus status) {
        this.success = success;
        this.data = data;
        this.error = error;
        this.message = message;
        this.status = status;
    }
    public ResponseDto(Boolean success, Object data, String message, HttpStatus status) {
        this.success = success;
        this.data = data;
        this.message = message;
        this.status = status;
    }
}
