package com.sysdream.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class ReviewImageDto implements Serializable {

    private static final long serialVersionUID = 8494878551944343466L;

    @NotNull(message = "accept value can not be null")
    private Boolean accept;
    // this bad boy is optional in case we want to update the user that suggested the photo, also to be omitted in case accept is True (no rejection at all in that case)
    private String rejectionReason;
}
