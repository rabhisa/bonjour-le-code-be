package com.sysdream.exception_handler;

import com.sysdream.dto.ResponseDto;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.io.IOException;
import java.util.StringJoiner;

@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice
public class WebRestControllerAdvice {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseDto handleHttpMessageNotReadableException(HttpMessageNotReadableException ex) {
        Throwable throwable = ex.getCause().getCause() == null ? ex.getCause() : ex.getCause().getCause();
        return ResponseDto.builder()
                .data(null)
                .error("Http message not readable")
                .success(Boolean.FALSE)
                .message("Wrong input, caused by : " + throwable.getMessage())
                .status(HttpStatus.BAD_REQUEST)
                .build();
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(IOException.class)
    public ResponseDto handleIOException(IOException ex) {
        Throwable throwable = ex.getCause().getCause() == null ? ex.getCause() : ex.getCause().getCause();
        return ResponseDto.builder()
                .data(null)
                .error("IO Exception probably while or processing some file")
                .success(Boolean.FALSE)
                .message("Error while Reading or Writing to some file, caused by : " + throwable.getMessage())
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseDto handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        StringJoiner sb = new StringJoiner("; ");
        // handle ViolationObjectError
        for (ObjectError objectError : ex.getBindingResult().getAllErrors()) {
            switch (objectError.getClass().getSimpleName()) {
                case "ViolationObjectError":
                    sb.add(objectError.getDefaultMessage());
                    break;
                case "ViolationFieldError":
                    sb.add(((FieldError) objectError).getField() + " " + objectError.getDefaultMessage());
                    break;
                default:
                    break;
            }
        }
        return ResponseDto.builder()
                .data(null)
                .error("Wrong args, please double check your doc")
                .success(Boolean.FALSE)
                .message(sb.toString())
                .status(HttpStatus.BAD_REQUEST)
                .build();

    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseDto handleMethodArgumentNotValidException(MethodArgumentTypeMismatchException ex) {
        Throwable throwable = ex.getCause().getCause() == null ? ex.getCause() : ex.getCause().getCause();
        return ResponseDto.builder()
                .data(null)
                .error("Http message not readable")
                .success(Boolean.FALSE)
                .message("Wrong input, caused by : " + throwable.getMessage())
                .status(HttpStatus.BAD_REQUEST)
                .build();
    }
}