package com.sysdream.mocks;

import com.github.javafaker.Faker;
import com.sysdream.model.ERole;
import com.sysdream.model.Role;
import com.sysdream.model.User;

import java.util.Arrays;
import java.util.HashSet;

public class UserMock {
    public static User getUserMock(){
        Faker faker = new Faker();
        return User.builder()
                .id(faker.random().nextLong(9999999))
                .username(faker.rickAndMorty().character())
                .email(faker.internet().emailAddress())
                .password(faker.internet().password())
                .roles(faker.random().nextBoolean() ? new HashSet<>(Arrays.asList(new Role(ERole.ROLE_USER))) :  new HashSet<>(Arrays.asList(new Role(ERole.ROLE_ADMIN)) ))
                .build();
    }

}

