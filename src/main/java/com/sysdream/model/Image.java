package com.sysdream.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "images")
@Getter
@Setter
@DynamicUpdate
@SQLDelete(sql = "UPDATE images SET deleted_date = now() WHERE id=?")
@Where(clause = "deleted_date is null")
public class Image extends BasicEntity implements Serializable {
    private static final long serialVersionUID = -3457520900768409644L;

    public Image(String name, String type, byte[] picByte) {
        this.name = name;
        this.type = type;
        this.picByte = picByte;
    }

    private String name;
    private String type;
    // this should be set only by admin, not validated by default
    private Boolean validated = Boolean.FALSE;

    private byte[] picByte;
    private Long userID;

    public Image(Long id, String name, String type, Boolean validated, byte[] decompressBytes, Long userID) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.validated = validated;
        this.picByte = decompressBytes;
        this.userID = userID;
    }
}