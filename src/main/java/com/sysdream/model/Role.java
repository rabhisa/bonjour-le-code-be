package com.sysdream.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "roles")
@DynamicUpdate
@SQLDelete(sql = "UPDATE roles SET deleted_date = now() WHERE id=?")
@Where(clause = "deleted_date is null")
public class Role extends BasicEntity implements Serializable {
    private static final long serialVersionUID = -2898742235858815563L;

    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private ERole name;

    public Role(ERole name) {
        this.name = name;
    }
}
