package com.sysdream.model;

import lombok.Builder;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table
@DynamicUpdate
@SQLDelete(sql = "UPDATE todays_data SET deleted_date = now() WHERE id=?")
@Where(clause = "deleted_date is null")
@Builder
public class TodaysData extends BasicEntity implements Serializable {
    private static final long serialVersionUID = -1751057234391196182L;

    private Image image;
    private String dateAsString;
    private String todaysFact;

}


