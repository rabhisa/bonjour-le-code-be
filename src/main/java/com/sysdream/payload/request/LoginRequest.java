package com.sysdream.payload.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
public class LoginRequest implements Serializable {
    private static final long serialVersionUID = 7425575552902485456L;

    @NotBlank
    private String username;

    @NotBlank
    @Size(max = 128)
    private String password;
}