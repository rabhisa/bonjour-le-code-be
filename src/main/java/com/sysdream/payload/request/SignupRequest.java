package com.sysdream.payload.request;

import com.sysdream.constraints.FieldMatch;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;


@FieldMatch.List({
        @FieldMatch(first = "password", second = "passwordConfirmation", message = "The password fields must match"),
        @FieldMatch(first = "email", second = "emailConfirmation", message = "The email fields must match")
})
@Data
public class SignupRequest implements Serializable {
    private static final long serialVersionUID = -6193581281709230723L;

    @NotBlank
    @Size(min = 3, max = 20)
    private String username;

    @NotBlank
    @Size(max = 50)
    @Email
    private String email;
    @NotBlank
    @Size(max = 50)
    @Email
    private String emailConfirmation;

    private String role;

    @NotBlank
    @Size(min = 6, max = 128)
    private String password;

    @NotBlank
    @Size(min = 6, max = 128)
    private String passwordConfirmation;
}