package com.sysdream.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
public class MessageResponse implements Serializable {
    private static final long serialVersionUID = 7444875363992748996L;

    private String message;
}