package com.sysdream.repo;

import com.sysdream.model.Image;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ImageRepository extends JpaRepository<Image, Long> {
    Page<Image> findAll(Pageable pageable);

    List<Image> findByUserID(Long userID);

    Optional<Image> findByIdAndUserID(Long imageId, Long userID);
}