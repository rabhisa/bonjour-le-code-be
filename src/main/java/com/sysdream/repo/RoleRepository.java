package com.sysdream.repo;

import java.util.Optional;

import com.sysdream.model.ERole;
import com.sysdream.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}