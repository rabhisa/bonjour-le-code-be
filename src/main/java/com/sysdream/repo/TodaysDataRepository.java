package com.sysdream.repo;

import com.sysdream.model.TodaysData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TodaysDataRepository extends JpaRepository<TodaysData, Long> {
    Optional<TodaysData> findFirstByOrderByCreatedDateDesc();
}
