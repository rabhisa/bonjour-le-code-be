package com.sysdream.scheduler;

import com.sysdream.controller.BonjourLeCodeController;
import com.sysdream.repo.ImageRepository;
import com.sysdream.repo.TodaysDataRepository;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.logging.Logger;

@Component
public class ScheduledTasks {
    private final ImageRepository imageRepository;
    private final TodaysDataRepository todaysDataRepository;

    public ScheduledTasks(ImageRepository imageRepository, TodaysDataRepository todaysDataRepository) {
        this.imageRepository = imageRepository;
        this.todaysDataRepository = todaysDataRepository;
    }

    @Scheduled(cron = "0 0 10 * * * ") // 10 am daily
    public void scheduleTaskWithCronExpression() {
        BonjourLeCodeController bonjourLeCodeController = new BonjourLeCodeController(imageRepository, todaysDataRepository);
        bonjourLeCodeController.generateTodaysData();
    }
}
