package com.sysdream.service;

import com.sysdream.config.security.JwtUtils;
import com.sysdream.model.ERole;
import com.sysdream.model.Role;
import com.sysdream.model.User;
import com.sysdream.payload.request.LoginRequest;
import com.sysdream.payload.request.SignupRequest;
import com.sysdream.payload.response.JwtResponse;
import com.sysdream.payload.response.MessageResponse;
import com.sysdream.repo.RoleRepository;
import com.sysdream.repo.UserRepository;
import com.sysdream.service.interfaces.AuthService;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class AuthServiceImpl implements AuthService {
    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder encoder;
    private final JwtUtils jwtUtils;

    public AuthServiceImpl(AuthenticationManager authenticationManager, UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder encoder, JwtUtils jwtUtils) {
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.encoder = encoder;
        this.jwtUtils = jwtUtils;
    }


    @Override
    public ResponseEntity<?> authenticateUser(@Valid LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                roles));
    }

    @Override
    public ResponseEntity<?> registerUser(@Valid SignupRequest signUpRequest) {
        ResponseEntity<MessageResponse> body = verifyRegisterUserInputReturnBodyCaseOfError(signUpRequest);
        if (body != null) return body;

        // Create new user's account
        User user = new User(signUpRequest.getUsername(),
                signUpRequest.getEmail(),
                encoder.encode(signUpRequest.getPassword()));

        String role = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (role == null) {
            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            switch (role) {
                case "ADMIN":
                    Optional<Role> roleOptional = roleRepository.findByName(ERole.ROLE_ADMIN);
                    Role myRole = new Role();
                    myRole = roleOptional.orElseGet(() -> roleRepository.save(new Role(ERole.ROLE_ADMIN)));
                    roles.add(myRole);

                    break;
                case "USER":
                    roleOptional = roleRepository.findByName(ERole.ROLE_USER);
                    myRole = new Role();
                    myRole = roleOptional.orElseGet(() -> roleRepository.save(new Role(ERole.ROLE_USER)));
                    roles.add(myRole);

                    break;
                default:
                    throw new RuntimeException("Error: Role is not found.");

            }

        }

        user.setRoles(roles);
        userRepository.save(user);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }

    @Nullable
    private ResponseEntity<MessageResponse> verifyRegisterUserInputReturnBodyCaseOfError(SignupRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername()))
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));


        if (userRepository.existsByEmail(signUpRequest.getEmail()))
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));

        return null;
    }
}
