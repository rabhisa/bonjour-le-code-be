package com.sysdream.service;

import com.sysdream.dto.ResponseDto;
import com.sysdream.dto.ReviewImageDto;
import com.sysdream.model.Image;
import com.sysdream.repo.ImageRepository;
import com.sysdream.repo.UserRepository;
import com.sysdream.service.interfaces.ImageService;
import com.sysdream.utils.StringUtils;
import io.github.cdimascio.dotenv.Dotenv;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.sysdream.utils.FileUtils.compressBytes;
import static com.sysdream.utils.FileUtils.decompressBytes;
import static com.sysdream.utils.ImageUtils.canUpdateOrDeleteImage;

@Service
public class ImageServiceImpl implements ImageService {
    private final ImageRepository imageRepository;
    private final UserRepository userRepository;

    public ImageServiceImpl(ImageRepository imageRepository, UserRepository userRepository) {
        this.imageRepository = imageRepository;
        this.userRepository = userRepository;
    }

    public static List<String> LIST_POSSIBLE_FILETYPES;
    public static List<String> LIST_POSSIBLE_EXTENSIONS;
    public static int FILE_MAX_LENGTH;
    public static int FILE_NAME_MAX_LENGTH;

    // dotenv keys
    private static final String LIST_POSSIBLE_FILETYPES_KEY = "LIST_POSSIBLE_FILETYPES";
    private static final String LIST_POSSIBLE_EXTENSIONS_KEY = "LIST_POSSIBLE_EXTENSIONS";
    private static final String FILE_MAX_LENGTH_KEY = "FILE_MAX_LENGTH";
    private static final String FILE_NAME_MAX_LENGTH_KEY = "FILE_NAME_MAX_LENGTH";

    // static block to preload env data
    static {
        Dotenv dotenv = Dotenv.load();
        // file types
        String possibleFileTypes = dotenv.get(LIST_POSSIBLE_FILETYPES_KEY).trim().toUpperCase();
        assert possibleFileTypes != null;
        LIST_POSSIBLE_FILETYPES = Arrays.asList(possibleFileTypes.split(", "));
        // file extensions
        String possibleExtensions = dotenv.get(LIST_POSSIBLE_EXTENSIONS_KEY).trim().toUpperCase();
        assert possibleExtensions != null;
        LIST_POSSIBLE_EXTENSIONS = Arrays.asList(possibleExtensions.split(", "));
        // file max length
        FILE_MAX_LENGTH = Integer.parseInt(Objects.requireNonNull(dotenv.get(FILE_MAX_LENGTH_KEY).trim()));
        // file name max length
        FILE_NAME_MAX_LENGTH = Integer.parseInt(Objects.requireNonNull(dotenv.get(FILE_NAME_MAX_LENGTH_KEY).trim()));
    }

    @Override
    public ResponseEntity uploadImage(Authentication authentication, @Valid MultipartFile file) throws IOException {
        // check input
        ResponseDto responseDto = isValidFile(file);
        if (!responseDto.getSuccess())
            return new ResponseEntity<>(new ResponseDto(Boolean.FALSE, responseDto.getMessage(), "uploadImage", HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);

        byte[] myFileCompressedBytes = compressBytes(file.getBytes());
        Long userID = ((UserDetailsImpl) authentication.getPrincipal()).getId();
        Image img = new Image(file.getOriginalFilename(), file.getContentType(), Boolean.FALSE, myFileCompressedBytes, userID);
        imageRepository.save(img);
        // after saving the file in repo we need to get the file to make sure all was good
        Optional<Image> savedImgOptional = imageRepository.findById(img.getId());
        if (!savedImgOptional.isPresent()) // most likely this wont happen
            return new ResponseEntity<>(new ResponseDto(Boolean.FALSE, "Could not retrieve file after it was saved", "uploadImage", HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
        Image savedImg = savedImgOptional.get();
        // assuming we are not updating the name while saving
        if (!savedImg.getName().equals(file.getOriginalFilename()) || !Arrays.equals(savedImg.getPicByte(), myFileCompressedBytes) || savedImg.getType().equals(file.getName())) {
            // this is to check that extension or size or name wont change, also assume that the server do some dynamic checks and remove the file if it a threat
            // if data is updated let's remove the file
            imageRepository.delete(img);
            return new ResponseEntity<>(new ResponseDto(Boolean.FALSE, "the file data was changed after it was saved", "uploadImage", HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return new ResponseEntity<>(new ResponseDto(Boolean.TRUE, img, "uploadImage", HttpStatus.CREATED), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity getImage(Authentication authentication, Long imageId) throws IOException {
        final Optional<Image> retrievedImageOptional = imageRepository.findByIdAndUserID(imageId, ((UserDetailsImpl) authentication.getPrincipal()).getId());
        if (!retrievedImageOptional.isPresent())
            return new ResponseEntity<>(new ResponseDto(Boolean.FALSE, "Image not found or don't belong to you", "getImage", HttpStatus.NOT_FOUND), HttpStatus.NOT_FOUND);
        Image retrievedImage = retrievedImageOptional.get();
        Image img = new Image(retrievedImage.getId(), retrievedImage.getName(), retrievedImage.getType(),
                retrievedImage.getValidated(), decompressBytes(retrievedImage.getPicByte()), retrievedImage.getUserID());
        return new ResponseEntity<>(new ResponseDto(Boolean.TRUE, img, "getImage", HttpStatus.OK), HttpStatus.OK);
    }

    @Override
    public ResponseEntity updateImage(Authentication authentication, Long imageId, MultipartFile file) throws IOException {
        final Optional<Image> retrievedImage = imageRepository.findById(imageId);
        if (!retrievedImage.isPresent())
            return new ResponseEntity<>(new ResponseDto(Boolean.FALSE, null, "updateImage", HttpStatus.NOT_FOUND), HttpStatus.NOT_FOUND);
        // if img exists check if it can be updated
        ;
        if (!canUpdateOrDeleteImage(authentication, retrievedImage.get().getId()))
            return new ResponseEntity<>(new ResponseDto(Boolean.FALSE, "Only image owner and admin can update", "updateImage", HttpStatus.FORBIDDEN), HttpStatus.FORBIDDEN);
        Image img = new Image(file.getOriginalFilename(), file.getContentType(),
                compressBytes(file.getBytes()));
        img.setId(imageId);
        imageRepository.save(img);
        return new ResponseEntity<>(new ResponseDto(Boolean.TRUE, img, "uploadImage", HttpStatus.OK), HttpStatus.OK);
    }

    @Override
    public ResponseEntity deleteImage(Authentication authentication, Long imageId) {
        final Optional<Image> retrievedImage = imageRepository.findById(imageId);
        if (!retrievedImage.isPresent())
            return new ResponseEntity<>(new ResponseDto(Boolean.FALSE, null, "deleteImage", HttpStatus.NOT_FOUND), HttpStatus.NOT_FOUND);
        if (!canUpdateOrDeleteImage(authentication, imageId))
            return new ResponseEntity<>(new ResponseDto(Boolean.FALSE, "Only image owner and admin can delete", "deleteImage", HttpStatus.FORBIDDEN), HttpStatus.FORBIDDEN);
        imageRepository.delete(retrievedImage.get());
        return new ResponseEntity<>(new ResponseDto(Boolean.TRUE, null, "deleteImage", HttpStatus.ACCEPTED), HttpStatus.ACCEPTED);
    }

    @Override
    public ResponseEntity reviewImage(Long imageId, ReviewImageDto dto) {
        final Optional<Image> retrievedImage = imageRepository.findById(imageId);
        if (!retrievedImage.isPresent())
            return new ResponseEntity<>(new ResponseDto(Boolean.FALSE, null, "reviewImage", HttpStatus.NOT_FOUND), HttpStatus.NOT_FOUND);
        Image img = retrievedImage.get();
        img.setValidated(dto.getAccept());
        imageRepository.save(img);
        return new ResponseEntity<>(new ResponseDto(Boolean.TRUE, img, "reviewImage", HttpStatus.OK), HttpStatus.OK);
    }

    @Override
    public ResponseEntity listImagesByUser(Authentication authentication) {
        List<Image> images = imageRepository.findByUserID(((UserDetailsImpl) authentication.getPrincipal()).getId());
        return new ResponseEntity<>(new ResponseDto(Boolean.TRUE, images, "listImages", HttpStatus.OK), HttpStatus.OK);

    }

    @Override
    public ResponseEntity listAllImages() {
        List<Image> images = imageRepository.findAll();
        return new ResponseEntity<>(new ResponseDto(Boolean.TRUE, images, "listImages", HttpStatus.OK), HttpStatus.OK);
    }


    private ResponseDto isValidFile(MultipartFile file) throws IOException {
        ResponseDto responseDto;
        if (!(responseDto = hasValidName(file.getOriginalFilename())).getSuccess())
            return responseDto;
        if (!(responseDto = hasValidType(Objects.requireNonNull(file.getContentType()))).getSuccess())
            return responseDto;
        if (!(responseDto = hasValidSize(file.getBytes())).getSuccess())
            return responseDto;
        return new ResponseDto(Boolean.TRUE);
    }

    private ResponseDto hasValidSize(byte[] bytes) {
        // todo debug here and check what bytes.length represent then compare it to our max length
        int length = bytes.length;
        if (length <= FILE_MAX_LENGTH)
            return new ResponseDto(Boolean.TRUE);
        return new ResponseDto(Boolean.FALSE, String.format("File size is %s, while max length is %s", length, FILE_MAX_LENGTH));
    }

    private ResponseDto hasValidType(String contentType) {
        if (!LIST_POSSIBLE_FILETYPES.contains(contentType.toUpperCase()))
            return new ResponseDto(Boolean.FALSE, StringUtils.parseErrorValueIsNotIsListOfPossibleValues("Content Type", LIST_POSSIBLE_FILETYPES.toString()));
        return new ResponseDto(Boolean.TRUE);
    }

    private ResponseDto hasValidName(String fileName) {
        if (fileName.length() > FILE_NAME_MAX_LENGTH)
            return new ResponseDto(Boolean.FALSE, String.format("Filename length is greater than max value, i.e: %s", FILE_NAME_MAX_LENGTH));
        String fileNameExtension = StringUtils.getFileExtensionFromFileName(fileName);
        if (!LIST_POSSIBLE_EXTENSIONS.contains(fileNameExtension.toUpperCase()))
            return new ResponseDto(Boolean.FALSE, StringUtils.parseErrorValueIsNotIsListOfPossibleValues("File Name extension", LIST_POSSIBLE_EXTENSIONS.toString()));
        return new ResponseDto(Boolean.TRUE);
    }


}
