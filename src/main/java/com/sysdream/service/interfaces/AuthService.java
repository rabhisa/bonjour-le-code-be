package com.sysdream.service.interfaces;

import com.sysdream.payload.request.LoginRequest;
import com.sysdream.payload.request.SignupRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

public interface AuthService {
    ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest);
    ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest);
}
