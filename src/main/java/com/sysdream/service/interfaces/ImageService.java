package com.sysdream.service.interfaces;

import com.sysdream.dto.ReviewImageDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;

public interface ImageService {
    ResponseEntity uploadImage(Authentication authentication, @RequestParam("imageFile") @Valid MultipartFile file) throws IOException;
    ResponseEntity getImage(Authentication authentication, @PathVariable("imageName") Long imageId) throws IOException;

    ResponseEntity updateImage(Authentication authentication, Long imageId, MultipartFile file) throws IOException;

    ResponseEntity deleteImage(Authentication authentication, Long imageId);

    ResponseEntity reviewImage(Long imageId, ReviewImageDto dto);

    ResponseEntity listImagesByUser(Authentication authentication);

    ResponseEntity listAllImages();
}
