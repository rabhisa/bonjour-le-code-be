package com.sysdream.utils;

import com.sysdream.model.ERole;
import com.sysdream.service.UserDetailsImpl;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;

public class ImageUtils {
    public static Boolean canUpdateOrDeleteImage(Authentication authentication, Long imageID) {
        return (createdByUser(authentication, imageID) || isAdmin(authentication));
    }

    public static boolean isAdmin(Authentication authentication) {
        return ((List<GrantedAuthority>) authentication.getAuthorities()).stream().anyMatch(x -> x.getAuthority().equals(ERole.ROLE_ADMIN.toString()));
    }

    public static boolean createdByUser(Authentication authentication, Long imageID) {
        return imageID.equals(((UserDetailsImpl) authentication.getPrincipal()).getId());
    }
}
