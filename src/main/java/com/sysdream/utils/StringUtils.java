package com.sysdream.utils;

public class StringUtils {
    public static String getFileExtensionFromFileName(String fileName){
        return fileName.substring(fileName.lastIndexOf(".") + 1);
    }

    public static String parseErrorValueIsNotIsListOfPossibleValues(String field, String possibleValues){
        return String.format("%s is not in the possible values, i.e: %s", field, possibleValues);
    }
}
